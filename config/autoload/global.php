<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'doctrine' => [
        'connection' => [
            // default connection name
            'orm_default' => [
                'driver_class' => \Doctrine\DBAL\Driver\PDOSqlite\Driver::class,
                'params' => [
                    'path'     => __DIR__ . '/../../data/simple_chat.db',
                ],
            ]
        ],
    ],
    'service_manager' => [
        'factories' => [
            'doctrine.entitymanager.orm_default' => \ContainerInteropDoctrine\EntityManagerFactory::class,
        ]
    ]
];
