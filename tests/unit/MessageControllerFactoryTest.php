<?php

use Application\Controller\Factory\MessageControllerFactory;
use Application\Controller\MessageController;
use Application\InputFilter\MessageInputFilter;
use Application\Service\MessageService;
use Application\Service\MessageServiceInterface;
use Mockery\MockInterface;
use Zend\InputFilter\InputFilterPluginManager;
use Zend\ServiceManager\ServiceManager;

class MessageControllerFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var MessageControllerFactory
     */
    protected $factory;

    /** @var ServiceManager|MockInterface */
    protected $serviceManager;

    protected function _before()
    {
        $this->serviceManager = \Mockery::mock(ServiceManager::class);

        $this->serviceManager->shouldReceive('get')
            ->with(MessageServiceInterface::class)
            ->andReturn(\Mockery::mock(MessageService::class));

        $inputFilterManager = \Mockery::mock(InputFilterPluginManager::class);

        $inputFilterManager->shouldReceive('get')
            ->with(MessageInputFilter::class)
            ->andReturn(\Mockery::mock(MessageInputFilter::class));

        $this->serviceManager->shouldReceive('get')
            ->with('InputFilterManager')
            ->andReturn($inputFilterManager);

        $this->factory = new MessageControllerFactory();
    }

    protected function _after()
    {
        Mockery::close();
    }

    public function testValidMessageControllerInstance()
    {
        $controller = ($this->factory)($this->serviceManager);

        $this->assertInstanceOf(MessageController::class, $controller);
    }
}