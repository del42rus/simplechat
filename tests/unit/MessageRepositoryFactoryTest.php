<?php

use Application\Persistence\Doctrine\Repository\Factory\MessageRepositoryFactory;
use Application\Persistence\Doctrine\Repository\MessageRepository;
use Doctrine\ORM\EntityManager;
use Mockery\MockInterface;
use Zend\ServiceManager\ServiceManager;

class MessageRepositoryFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var MessageRepositoryFactory
     */
    protected $factory;

    /**
     * @var ServiceManager|MockInterface
     */
    protected $serviceManager;
    
    protected function _before()
    {
        $this->serviceManager = \Mockery::mock(ServiceManager::class);

        $this->serviceManager->shouldReceive('get')
            ->with('doctrine.entitymanager.orm_default')
            ->andReturn(\Mockery::mock(EntityManager::class));

        $this->factory = new MessageRepositoryFactory();
    }

    protected function _after()
    {
        Mockery::close();
    }

    public function testValidMessageRepositoryInstance()
    {
        $repository = ($this->factory)($this->serviceManager);

        $this->assertInstanceOf(MessageRepository::class, $repository);
    }
}