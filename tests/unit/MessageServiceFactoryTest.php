<?php

use Application\Domain\Repository\MessageRepositoryInterface;
use Application\Persistence\Doctrine\Repository\MessageRepository;
use Application\Service\Factory\MessageServiceFactory;
use Application\Service\MessageService;
use Zend\ServiceManager\ServiceManager;
use Mockery\MockInterface;

class MessageServiceFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var MessageServiceFactory
     */
    protected $factory;

    /**
     * @var ServiceManager|MockInterface
     */
    protected $serviceManager;

    protected function _before()
    {
        $this->serviceManager = \Mockery::mock(ServiceManager::class);

        $this->serviceManager->shouldReceive('get')
            ->with(MessageRepositoryInterface::class)
            ->andReturn(\Mockery::mock(MessageRepository::class));

        $this->factory = new MessageServiceFactory();
    }

    protected function _after()
    {
        Mockery::close();
    }

    public function testValidMessageServiceInstance()
    {
        $service = ($this->factory)($this->serviceManager);

        $this->assertInstanceOf(MessageService::class, $service);
    }
}