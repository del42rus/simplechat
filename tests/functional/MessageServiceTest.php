<?php

use Application\Domain\Entity\Message;
use Application\Service\MessageServiceInterface;

class MessageServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    /**
     * @var MessageServiceInterface
     */
    protected $messageService;
    
    protected function _before()
    {
        $this->messageService = $this->tester->grabServiceFromContainer(MessageServiceInterface::class);
    }

    protected function _after()
    {
        $this->messageService = null;
    }

    // tests
    public function testSaveMethod()
    {
        $message = new Message();
        $message->setText('test message');

        $this->messageService->save($message);

        $this->tester->seeInDatabase('messages', ['text' => 'test message']);
    }

    public function testRemoveOldMessagesMethod()
    {
        $this->messageService->removeOldMessages(1);

        $this->tester->seeInDatabase('messages', ['text' => 'text2']);
    }
}