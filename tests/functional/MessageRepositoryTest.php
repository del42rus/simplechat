<?php

use Application\Domain\Repository\MessageRepositoryInterface;

class MessageRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    /**
     * @var \Application\Domain\Repository\MessageRepositoryInterface
     */
    protected $messageRepository;
    
    protected function _before()
    {
        $this->messageRepository = $this->tester->grabServiceFromContainer(MessageRepositoryInterface::class);
    }

    protected function _after()
    {
        $this->messageRepository = null;
    }

    // tests
    public function testGetMessagesCountMethod()
    {
        $this->assertEquals(2, $this->messageRepository->getMessagesCount());
    }
}