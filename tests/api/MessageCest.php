<?php 

class MessageCest
{
    public function _before(ApiTester $I)
    {
    }

    public function getList(ApiTester $I)
    {
        $I->sendGET('/messages');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('$.data');
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'text' => 'string',
            'created_at' => 'string',
        ], '$.data[*]');
    }

    public function create(ApiTester $I)
    {
        $I->sendPOST('/messages', ['text' => 'test message']);
        $I->seeResponseCodeIs(201);
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'text' => 'string',
            'created_at' => 'string',
        ]);
        $I->seeResponseContainsJson(['text' => 'test message']);
    }
}
