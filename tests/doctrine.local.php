<?php
return [
    'doctrine' => [
        'connection' => [
            // default connection name
            'orm_default' => [
                'driver_class' => \Doctrine\DBAL\Driver\PDOSqlite\Driver::class,
                'params' => [
                    'path'     => __DIR__ . '/../data/simple_chat_test.db',
                ],
            ]
        ],
    ],
];