## Installation

```bash
$ composer install
$ npm install
$ npm run dev
$ php -S 127.0.0.1:9000 -t public
# Run Codeception tests:
$ vendor/bin/codecept run
```
