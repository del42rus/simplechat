import Vue from 'vue';

import App from './App.vue';

require('./styles.scss');

new Vue({
    el: '#app',
    render: h => h(App)
});