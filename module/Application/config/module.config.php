<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Controller\Factory\MessageControllerFactory;
use Application\Domain\Repository\MessageRepositoryInterface;
use Application\InputFilter\MessageInputFilter;
use Application\Persistence\Doctrine\Repository\Factory\MessageRepositoryFactory;
use Application\Service\Factory\MessageServiceFactory;
use Application\Service\MessageServiceInterface;
use Gedmo\Timestampable\TimestampableListener;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'messages' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/messages[/:id]',
                    'constraints' => [
                        'id' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\MessageController::class,
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\MessageController::class => MessageControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            MessageRepositoryInterface::class => MessageRepositoryFactory::class,

            MessageServiceInterface::class => MessageServiceFactory::class,
        ]
    ],
    'input_filters' => [
        'factories' => [
            MessageInputFilter::class => InvokableFactory::class
        ]
    ],
    'doctrine' => [
        'event_manager' => [
            'orm_default' => [
                'subscribers' => [
                    TimestampableListener::class
                ],
            ],
        ],
        'driver' => [
            'orm_default' => [
                'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver',
                ],
            ],
            __NAMESPACE__ . '_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\YamlDriver::class,
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' => [__DIR__ . '/yml']
            ],
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
