<?php

namespace Application\Persistence\Doctrine\Repository;

use Application\Domain\Entity\Message;
use Application\Domain\Repository\MessageRepositoryInterface;

class MessageRepository extends AbstractRepository implements MessageRepositoryInterface
{
    protected $entityClass = Message::class;

    public function getMessagesCount()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('COUNT(m.id)')
            ->from($this->entityClass, 'm');

        $query = $queryBuilder->getQuery();

        return $query->getSingleScalarResult();
    }
}