<?php

namespace Application\Persistence\Doctrine\Repository\Factory;

use Application\Persistence\Doctrine\Repository\MessageRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class MessageRepositoryFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new MessageRepository($entityManager);
    }
}