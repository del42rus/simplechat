<?php

namespace Application\Domain\Repository;

use Application\Domain\Entity\AbstractEntity;

interface RepositoryInterface
{
    public function getById($id);
    public function getAll();
    public function getBy($conditions = [], $order = [], $limit = null, $offset = null);
    public function getOneBy($conditions = [], $order = []);
    public function persist(AbstractEntity $entity) : RepositoryInterface;
    public function remove(AbstractEntity $entity) : RepositoryInterface;
    public function begin() : RepositoryInterface;
    public function commit() : RepositoryInterface;
}