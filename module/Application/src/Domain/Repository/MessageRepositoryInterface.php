<?php

namespace Application\Domain\Repository;

interface MessageRepositoryInterface extends RepositoryInterface
{
    public function getMessagesCount();
}