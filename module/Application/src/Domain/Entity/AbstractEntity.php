<?php

namespace Application\Domain\Entity;

use Zend\Hydrator\ClassMethodsHydrator;
use Zend\Hydrator\Filter\FilterComposite;
use Zend\Hydrator\Filter\FilterInterface;
use Zend\Hydrator\Filter\FilterProviderInterface;
use Zend\Hydrator\Filter\GetFilter;
use Zend\Hydrator\Filter\MethodMatchFilter;

abstract class AbstractEntity implements FilterProviderInterface
{
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getFilter() : FilterInterface
    {
        $composite = new FilterComposite([], [new GetFilter(), new MethodMatchFilter('getArrayCopy')]);

        return $composite;
    }

    public function getArrayCopy()
    {
        return (new ClassMethodsHydrator())
            ->extract($this);
    }
}