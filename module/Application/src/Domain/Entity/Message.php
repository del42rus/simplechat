<?php

namespace Application\Domain\Entity;

use Zend\Hydrator\ClassMethodsHydrator;
use Zend\Hydrator\Strategy\DateTimeFormatterStrategy;

/**
 * Message
 */
class Message extends AbstractEntity
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getArrayCopy()
    {
        $hydrator = new ClassMethodsHydrator();

        $hydrator->addStrategy('createdAt', new DateTimeFormatterStrategy('Y-m-d H:i:s'));

        return $hydrator->extract($this);
    }
}
