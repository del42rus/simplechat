<?php

namespace Application\InputFilter;

use Zend\Filter\StringTrim;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

class MessageInputFilter extends InputFilter
{
    public function init()
    {
        $this->add([
            'name' => 'text',
            'required' => true,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 1,
                        'max' => 140,
                    ],
                ],
            ],
            'filters' => [
                [
                    'name' => StringTrim::class
                ]
            ]
        ]);
    }
}