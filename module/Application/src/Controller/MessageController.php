<?php

namespace Application\Controller;

use Application\Domain\Entity\Message;
use Application\InputFilter\MessageInputFilter;
use Application\Service\MessageServiceInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class MessageController extends AbstractRestfulController
{
    private $messageService;

    private $inputFilter;

    public function __construct(
        MessageServiceInterface $messageService,
        MessageInputFilter $messageInputFilter
    ){
        $this->messageService = $messageService;
        $this->inputFilter = $messageInputFilter;
    }

    public function getList()
    {
        $messages = $this->messageService->getBy([], ['createdAt' => 'desc']);

        $data = [];

        /** @var Message $message */
        foreach ($messages as $message) {
            $data[] = $message->getArrayCopy();
        }

        return new JsonModel([
            'data' => $data
        ]);
    }

    public function create($data)
    {
        $this->inputFilter->setData($data);

        if (!$this->inputFilter->isValid()) {
            $this->getResponse()->setStatusCode(422);

            return new JsonModel([
                'status' => 422,
                'title' => 'Unprocessable Entity',
                'detail' => 'Validation failed.',
                'errors' => $this->inputFilter->getMessages(),
            ]);
        }

        $message = new Message();
        $message->setText($this->inputFilter->getValue('text'));

        $this->messageService->save($message);

        $this->messageService->removeOldMessages(10);

        $this->getResponse()->setStatusCode(201);

        return new JsonModel($message->getArrayCopy());
    }
}