<?php

namespace Application\Controller\Factory;

use Application\Controller\MessageController;
use Application\InputFilter\MessageInputFilter;
use Application\Service\MessageServiceInterface;
use Interop\Container\ContainerInterface;

class MessageControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var MessageServiceInterface $messageService */
        $messageService = $container->get(MessageServiceInterface::class);

        /** @var MessageInputFilter $inputFilter */
        $inputFilter = $container->get('InputFilterManager')->get(MessageInputFilter::class);

        return new MessageController(
            $messageService,
            $inputFilter
        );
    }
}