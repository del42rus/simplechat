<?php

namespace Application\Service\Factory;

use Application\Domain\Repository\MessageRepositoryInterface;
use Application\Service\MessageService;
use Interop\Container\ContainerInterface;

class MessageServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var MessageRepositoryInterface $messageRepository */
        $messageRepository = $container->get(MessageRepositoryInterface::class);

        return new MessageService($messageRepository);
    }
}