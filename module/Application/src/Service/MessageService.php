<?php

namespace Application\Service;

use Application\Domain\Entity\Message;
use Application\Domain\Repository\MessageRepositoryInterface;

class MessageService implements MessageServiceInterface
{
    private $messageRepository;

    public function __construct(MessageRepositoryInterface $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function save(Message $message)
    {
        $this->messageRepository->begin()
            ->persist($message)
            ->commit();
    }

    public function removeOldMessages($maxCount)
    {
        $messageCount = $this->messageRepository->getMessagesCount();

        if ($messageCount > $maxCount) {
            $olderMessages = $this->messageRepository->getBy([], ['createdAt' => 'asc'], $messageCount - $maxCount);

            $this->messageRepository->begin();

            /** @var Message $message */
            foreach ($olderMessages as $message) {
                $this->messageRepository->remove($message);
            }

            $this->messageRepository->commit();
        }
    }

    public function __call($method, $parameters)
    {
        return $this->messageRepository->$method(...$parameters);
    }
}