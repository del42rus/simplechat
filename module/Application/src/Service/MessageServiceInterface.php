<?php

namespace Application\Service;

use Application\Domain\Entity\Message;

interface MessageServiceInterface
{
    public function save(Message $message);
    public function removeOldMessages($maxCount);
}